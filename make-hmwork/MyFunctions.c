int POW(int base,int exponent)
{
	int result = 1;
	int i;
	if(exponent == 0)
		return 1;
	for(i=0; i<exponent; i++)
	{
		result *= base;
	}
	return result;
}
