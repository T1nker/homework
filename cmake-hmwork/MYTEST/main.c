#include<stdio.h>
#include<stdlib.h>
#include<config.h>

#ifdef USE_MINE
	#include <somecal.h>
#endif

int main(int argc, char *argv[])
{
	if (argc < 3){
        printf("Usage: %s base exponent \n", argv[0]);
        return 1;
    }
        int a = atof(argv[1]);
	int b = atoi(argv[2]);
	int answer = calcu(a,b);
	printf("%d and %d is %d\n",a,b,answer);
	return 0;
}
