# CMake generated Testfile for 
# Source directory: /home/tinker/Hmwork/cmake-hmwork/MYTEST
# Build directory: /home/tinker/Hmwork/cmake-hmwork/MYTEST
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_2_5 "demo" "2" "5")
set_tests_properties(test_2_5 PROPERTIES  PASS_REGULAR_EXPRESSION "is 31")
add_test(test_4_4 "demo" "4" "4")
set_tests_properties(test_4_4 PROPERTIES  PASS_REGULAR_EXPRESSION "is 0")
add_test(test_1_8 "demo" "1" "8")
set_tests_properties(test_1_8 PROPERTIES  PASS_REGULAR_EXPRESSION "is 43")
add_test(test_400_400 "demo" "400" "400")
set_tests_properties(test_400_400 PROPERTIES  PASS_REGULAR_EXPRESSION "is 0")
add_test(test_-4_3 "demo" "-4" "3")
set_tests_properties(test_-4_3 PROPERTIES  PASS_REGULAR_EXPRESSION "is 3")
subdirs(mylib)
